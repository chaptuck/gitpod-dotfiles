# Library for displaying git prompt data

bind '"\e[A": history-search-backward'
bind '"\e[B": history-search-forward'

ddbt() {
  if [[ -z $DBT_VERSION ]]; then
    echo 'DBT_VERSION not set'
    return
  fi

  if [[ -z $DBT_ADAPTER ]]; then
    echo 'DBT_ADAPTER not set, defaulting to dbt-postgres'
  fi

  ADAPTER="${DBT_ADAPTER:-dbt-postgres}"

  docker run -it \
    --user "$(id -u)" \
    --volume "$(pwd):/usr/app" \
    --volume "$HOME/.dbt:/.dbt" \
    ghcr.io/dbt-labs/$ADAPTER:$DBT_VERSION $@
}

dsql() {
  if [[ -z $PGPASSWORD ]]; then
    echo 'PGPASSWORD not set'
    return
  fi

  if [[ -z $PGHOST ]]; then
    echo 'PGHOST not set, defaulting to localhost'
  fi

  if [[ -z $PGPORT ]]; then
    echo 'PGPORT not set, defaulting to 5432'
  fi

  if [[ -z $PGUSER ]]; then
    echo 'PGUSER not set, defaulting to postgres'
  fi

  PGHOST="${PGHOST:-localhost}"
  PGPORT="${PGPORT:-5432}"
  PGUSER="${PGUSER:-postgres}"

  docker run -it \
  --volume "$HOME/.psqlrc:/root/.psqlrc" \
  --volume "$HOME/.psql_vol:/tmp" \
  -e "PGPASSWORD=$PGPASSWORD" \
  -e "PGHOST=$PGHOST" \
  -e "PGUSER=$PGUSER" \
  -e "PGPORT=$PGPORT" \
  postgres psql
}

yaml() {
  ## Usage 
  ##   yaml ~/path/to/file.yaml "['yaml_key']"
  python3 -c "import yaml;print(yaml.safe_load(open('$1'))$2)"
}

db_deps() {
  # 1 - Profile
  # 2 - Target
  export PGPORT=$(yaml $HOME/.dbt/profiles.yml "['$1']['outputs']['$2']['port']";)
  export PGHOST=$(yaml $HOME/.dbt/profiles.yml "['$1']['outputs']['$2']['host']";)
  export PGUSER=$(yaml $HOME/.dbt/profiles.yml "['$1']['outputs']['$2']['user']";)
}

alias nvim="/opt/nvim-linux64/bin/nvim"

set -o vi
bind -q complete
export EDITOR="/opt/nvim-linux64/bin/nvim"
export VISUAL="/opt/nvim-linux64/bin/nvim"
